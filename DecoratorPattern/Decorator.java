public class Decorator implements IntefacePizza {
    private IntefacePizza pizza;

    public Decorator(IntefacePizza p) {
        pizza = p;
    }

    @Override
    public void Price() {
        pizza.Price();
    }

    @Override
    public void Label() {
        pizza.Label();
    }

    public void doubleTopping() {
        System.out.println("Pizza whith double topping.");
    }
}

