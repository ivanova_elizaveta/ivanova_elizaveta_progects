public class HawaiianPizzaDecorator extends Decorator {


    public HawaiianPizzaDecorator(IntefacePizza p) {
        super(p);
    }

    @Override
    public void Price() {
        super.Price();
        System.out.println("156");
    }

    @Override
    public void Label() {
        super.Label();
        System.out.println("Hawaiian Pizza");
    }

    public void doubleTopping() {
        System.out.println("Pizza whith double pineapple.");
    }


}
