public class Main {
    public static void main(String[] args) {

        Decorator d1 = new HawaiianPizzaDecorator(new MainPizza());

        d1.Label();
        d1.doubleTopping();
        d1.Price();
        System.out.println("\n");


        Decorator d2 = new FourSeasonsPizzaDecorator(new MainPizza());

        d2.Label();
        d2.doubleTopping();
        d2.Price();
    }
}
