public class FourSeasonsPizzaDecorator extends Decorator {
    public FourSeasonsPizzaDecorator(IntefacePizza p) {
        super(p);
    }

    @Override
    public void Price() {
        super.Price();
        System.out.println("192");
    }

    @Override
    public void Label() {
        super.Label();
        System.out.println("FourSeasons Pizza");
    }

    public void doubleTopping() {
        System.out.println("Pizza whith double cheese.");
    }
}
