public class Complex {
    private double x;
    private double y;

    public Complex(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double GetX(Complex a) {
        return a.x;
    }

    public double GetY(Complex a) {
        return a.y;
    }

    public Complex Add(Complex b) {
        Complex a = new Complex(this.x + GetX(b), this.y + GetY(b));
        return a;
    }

    public Complex Sub(Complex b) {
        Complex a = new Complex(this.x - GetX(b), this.y - GetY(b));
        return a;
    }

    public Complex Mul(Complex b) {
        Complex a = new Complex(this.x * GetX(b) - this.y * GetY(b), this.y * GetX(b) + this.x * GetY(b));
        return a;
    }

    public void Print() {
        System.out.printf("Result: %.2f,%+.2fi", this.x, this.y);
    }


}
