//Составить описание класса для представления комплексных чисел с возможностью
// задания вещественной и мнимой частей как числами типов double, так и целыми числами.
// Обеспечить выполнение операций сложения, вычитания и умножения комплексных чисел.

import java.awt.print.Printable;

public class Main {

    public static void main(String[] args) {

        Complex a = new Complex(3, 6);
        Complex b = new Complex(34, 7);
        Complex c = a.Add(b);
        Complex d = a.Sub(b);
        Complex f = a.Mul(b);

        c.Print();
        System.out.println("\n");
        d.Print();
        System.out.println("\n");
        f.Print();
    }
}
