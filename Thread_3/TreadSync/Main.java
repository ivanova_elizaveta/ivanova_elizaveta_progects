import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Message target = new Message();
        final Messanger messanger = new Messanger("Hello!", target);
        final List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            Thread thread1 = new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    messanger.test1();
                }
            });
            Thread thread2 = new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    messanger.test2();
                }
            });
            thread1.start();
            thread2.start();
            threads.add(thread1);
            threads.add(thread2);
        }


        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
