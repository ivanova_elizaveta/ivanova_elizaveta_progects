public class Message {
    void message(String msg) {
        System.out.println(msg);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Message transfer interrupted");
        }
        System.out.println("Message transfer completed");

    }
}
