public class Messanger implements Runnable {
    static String msg;
    static Message target;
    Thread t;

    public Messanger(String s, Message target) {
        msg = s;
        this.target = target;
        t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        synchronized (target) {
            target.message(msg);
        }
    }

    static synchronized void test1() {
        synchronized (target) {
            System.out.println("Message from static method: " + msg);
        }
    }

    synchronized void test2() {
        synchronized (this) {
            System.out.println("Message from non static method: " + msg);
        }
    }


}
