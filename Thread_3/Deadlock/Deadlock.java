public class Deadlock implements Runnable {

    FirstClass a = new FirstClass();
    SecondClass b = new SecondClass();

    Deadlock() {
        Thread.currentThread().setName("currentThread");
        Thread t = new Thread(this,"currentThread");
        t.start();

        a.first(b);

        System.out.println(" a.first(b)  ");
    }

    @Override
    public void run() {
        b.second(a);
        System.out.println("b.second(a)");

    }


    public static void main(String[] args) {
        new Deadlock();
    }
}
