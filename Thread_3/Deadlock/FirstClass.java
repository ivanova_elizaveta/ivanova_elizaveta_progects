public class FirstClass {

    synchronized void first(SecondClass b) {
        String name = Thread.currentThread().getName();
        System.out.println(name + "    FirstClass.first()");

        try {
            Thread.sleep(1000);

        } catch (InterruptedException e) {
            System.out.println("FirstClass");
        }

        System.out.println(name + "    SecondClass.last()");
        b.last();

    }


    synchronized void last() {
        System.out.println("  FirstClass.last()");
    }
}
