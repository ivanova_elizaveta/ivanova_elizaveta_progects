public class SecondClass {

    synchronized void second(FirstClass a) {

        String name = Thread.currentThread().getName();
        System.out.println(name + "    SecondClass.second()");

        try {
            Thread.sleep(1000);

        } catch (InterruptedException e) {
            System.out.println("SecondClass");
        }

        System.out.println(name + "    FirstClass.last()");
        a.last();

    }

    synchronized void last() {
        System.out.println("  FirstClass.last()");
    }
}
