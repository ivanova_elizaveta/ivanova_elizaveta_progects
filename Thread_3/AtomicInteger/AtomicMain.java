
public class AtomicMain {
    public static void main(String[] args) throws InterruptedException {

        ProcessingThread p = new ProcessingThread();
        Thread t1 = new Thread(p, "t1");
        t1.start();
        Thread t2 = new Thread(p, "t2");
        t2.start();
        t1.join();
        t2.join();

        System.out.println("Processing count= " + p.getCount());


    }
}
