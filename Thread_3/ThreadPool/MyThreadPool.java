import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class MyThreadPool {

    private volatile boolean isActive = false;
    private static final int PROCESSOR_COUNT = Runtime
            .getRuntime().availableProcessors() + 10;

    private final List<Thread> threads = new ArrayList<>();
    private final Deque<Exec> jobs = new ArrayDeque<>();


    private final Runnable runnable = () -> {
        while (isActive) {
            if (!jobs.isEmpty()) {
                Exec job = jobs.poll();
                if (job != null) {
                    System.out.println(String.format(
                            "Name: %s; Id: %s",
                            Thread.currentThread().getName(),
                            Thread.currentThread().getId()));
                    job.exec();
                }
            } else {
                Thread.yield();
            }
        }
    };

    public MyThreadPool() {
        this(PROCESSOR_COUNT);
    }

    public MyThreadPool(int count) {
        isActive = true;
        for (int i = 0; i < count; i++) {
            final Thread thread = new Thread(
                    runnable,
                    "MyThreadPool_" + i);
            thread.start();
            threads.add(thread);
        }
    }

    public void addJob(Exec job) {
        jobs.add(job);
    }

    public void stop() {
        isActive = false;
    }


}
