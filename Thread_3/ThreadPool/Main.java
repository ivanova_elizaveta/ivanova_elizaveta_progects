public class Main {

    private static final MyThreadPool MY_THREAD_POOL = new MyThreadPool();

    public static void main(String[] args) {
        MY_THREAD_POOL.addJob(() -> {
            System.out.println("1");
        });
        MY_THREAD_POOL.addJob(() -> {
            System.out.println("2");
        });
        MY_THREAD_POOL.addJob(() -> {
            System.out.println("3");
        });

        MY_THREAD_POOL.stop();


    }
}
