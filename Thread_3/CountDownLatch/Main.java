import java.util.concurrent.CountDownLatch;

public class Main {
    public static void main(String[] args) {

        CountDownLatch c = new CountDownLatch(5);
        System.out.println("Запуск потока исполнения");

        new MyThread(c);

        try {
            c.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Завершение потока исполнения");

    }
}
