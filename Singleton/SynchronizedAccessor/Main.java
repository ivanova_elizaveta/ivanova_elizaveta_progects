public class Main {
    public static void main(String[] args) {

        Singleton firstInstance = Singleton.getInstance();

        System.out.println(firstInstance.getClass());

        Singleton secondInstance = Singleton.getInstance();

        boolean f = firstInstance == secondInstance;
        System.out.println(f);

    }
}
