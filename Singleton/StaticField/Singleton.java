public class Singleton {

    public static final Singleton INSTANCE = new Singleton();

    public static Singleton getInstance(){

        return INSTANCE;

    }
}
