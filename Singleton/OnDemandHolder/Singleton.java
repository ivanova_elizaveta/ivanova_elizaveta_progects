public class Singleton {
    public static Singleton getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    public static class SingletonHolder {
        static final Singleton HOLDER_INSTANCE = new Singleton();
    }

}
