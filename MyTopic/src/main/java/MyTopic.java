import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;

import javax.jms.*;
import java.net.URI;

public class MyTopic {

    private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;
    private static String subject = "Topic";

    public static void main(String[] args) throws Exception {
        BrokerService broker = BrokerFactory.createBroker(new URI(
                "broker:(tcp://localhost:20000)"));
        broker.start();

        Connection connection = null;

        try{
            ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
            connection = connectionFactory.createConnection();
            Session session = connection.createSession(false,
                    Session.AUTO_ACKNOWLEDGE);

            Topic topic = session.createTopic(subject);

            MessageConsumer consumer1 = session.createConsumer(topic);
            consumer1.setMessageListener(new ConsumerMessageListener("Consumer1"));

            MessageConsumer consumer2 = session.createConsumer(topic);
            consumer2.setMessageListener(new ConsumerMessageListener("Consumer2"));

            connection.start();

            String str = "Message to Topik";
            Message msg = session.createTextMessage(str);
            MessageProducer producer = session.createProducer(topic);
            System.out.println("Sending text: '" + str + "'");
            producer.send(msg);

            Thread.sleep(3000);
            session.close();
        }
        finally {
            if (connection != null) {
                connection.close();
            }
            broker.stop();
        }



    }
}
