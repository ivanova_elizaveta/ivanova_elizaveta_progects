package com.SpringBootSecurity.service;

import com.SpringBootSecurity.Repository.UserRepository;
import com.SpringBootSecurity.model.Role;
import com.SpringBootSecurity.model.User;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
@WebMvcTest(UserServiceImpl.class)
class UserServiceTest {

    @MockBean
    UserRepository userRepository;
    @MockBean
    UserServiceImpl service;
    @Mock
    User user;
    List<User> userList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        service = new UserServiceImpl(userRepository);

        Role role = new Role(1L, "ADMIN");
        Set<Role> roles = new LinkedHashSet<>();
        roles.add(role);
        user = new User(1L, "login1", "password1", 44, roles);

        userList.add(user);
    }

    @Test
    void loadUserByUsername() {
        Mockito.when(service.loadUserByUsername("login1")).thenReturn(user);
    }

    @Test
    void findUserById() {
        Mockito.when(service.findUserById(1L)).thenReturn(user);
    }

    @Test
    void allUsers() {
        Mockito.when(service.allUsers()).thenReturn(userList);
    }

    @Test
    void saveUser() {
        Mockito.when(service.saveUser(user)).thenReturn(true);
    }
}