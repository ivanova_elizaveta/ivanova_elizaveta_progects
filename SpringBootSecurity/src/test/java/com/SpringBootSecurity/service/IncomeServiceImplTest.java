package com.SpringBootSecurity.service;

import com.SpringBootSecurity.Repository.IncomeRepository;
import com.SpringBootSecurity.model.Income;
import com.SpringBootSecurity.model.Role;
import com.SpringBootSecurity.model.User;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
@WebMvcTest(IncomeService.class)
class IncomeServiceImplTest {

    @MockBean
    IncomeRepository incomeRepository;
    @MockBean
    IncomeServiceImpl incomeService;
    @Mock
    Income income;
    User user;
    List<Income> incomeList = new ArrayList<>();
    List<Double> avr=new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        incomeService = new IncomeServiceImpl(incomeRepository);

        Role role = new Role(1L, "ADMIN");
        Set<Role> roles = new LinkedHashSet<>();
        roles.add(role);
        user = new User(1L, "login1", "password1", 44, roles);
        income = new Income(1, user, 345);

        incomeList.add(income);
    }

    @Test
    void addIncome() {
        Mockito.when(incomeService.addIncome(income)).thenReturn(income);
    }

    @Test
    void findByUserIdIs() {
        Mockito.when(incomeService.findByUserIdIs(1)).thenReturn(incomeList);
    }

    @Test
    void findAvgByUserId() {
        Mockito.when(incomeService.findAvgByUserId()).thenReturn(avr);
    }
}