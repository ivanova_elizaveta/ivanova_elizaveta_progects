package com.SpringBootSecurity.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "income")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Income {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "value")
    private int value;


}
