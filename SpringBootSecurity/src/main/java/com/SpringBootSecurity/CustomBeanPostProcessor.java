package com.SpringBootSecurity;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cglib.proxy.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class beanClass = bean.getClass();
        Object myBean = bean;
        final Object bean1 = myBean;

        if (Proxy.isProxyClass(beanClass)) {
            return bean;
        }

        List<Class<?>> interfaceList = getAllInterfaces(beanClass);

        Class[] interfaces = (interfaceList.toArray(new Class[interfaceList.size()]));

        if (beanClass.isAnnotationPresent(Service.class)) {

            myBean = Enhancer.create(beanClass, interfaces, new MethodInterceptor() {

                @Override
                public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                    Object target = bean1;
                    long before = System.nanoTime();
                    Object result = methodProxy.invoke(target, objects);
                    long after = System.nanoTime();
                    System.out.println("Working time: " + (after - before) + " nSec");
                    return result;
                }
            });

        }
        return myBean;
    }

    private List<Class<?>> getAllInterfaces(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        LinkedHashSet<Class<?>> interfacesFound = new LinkedHashSet<Class<?>>();
        getAllInterfaces(cls, interfacesFound);
        return new ArrayList<Class<?>>(interfacesFound);
    }

    private void getAllInterfaces(Class<?> cls, HashSet<Class<?>> interfacesFound) {
        while (cls != null) {
            Class<?>[] interfaces = cls.getInterfaces();
            for (Class<?> i : interfaces) {
                if (interfacesFound.add(i)) {
                    getAllInterfaces(i, interfacesFound);
                }
            }
            cls = cls.getSuperclass();
        }
    }

}
