package com.SpringBootSecurity.Repository;


import com.SpringBootSecurity.model.Income;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncomeRepository extends CrudRepository<Income, Long> {

    List<Income> findByUserIdIs(@Param("id") Integer id);

    @Query("select avg(value) from Income group by user.id")
    List<Double> findAvgByUserId();

}
