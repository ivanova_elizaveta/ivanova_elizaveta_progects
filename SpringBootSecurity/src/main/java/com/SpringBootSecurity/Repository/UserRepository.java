package com.SpringBootSecurity.Repository;


import com.SpringBootSecurity.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLogin(String username);
}
