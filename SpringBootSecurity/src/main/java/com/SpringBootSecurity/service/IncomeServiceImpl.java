package com.SpringBootSecurity.service;

import com.SpringBootSecurity.DoMeasure;
import com.SpringBootSecurity.Repository.IncomeRepository;
import com.SpringBootSecurity.model.Income;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("incomeService")
@Transactional
@NoArgsConstructor
public class IncomeServiceImpl implements IncomeService {

    private IncomeRepository incomeRepository;

    @Autowired
    public IncomeServiceImpl(IncomeRepository incomeRepository) {
        this.incomeRepository = incomeRepository;
    }

    @DoMeasure
    @Transactional
    @Override
    public Income addIncome(Income income) {
        Income i = incomeRepository.save(income);
        return i;
    }

    @DoMeasure
    @Transactional
    @Override
    public List<Income> findByUserIdIs(Integer id) {
        return incomeRepository.findByUserIdIs(id);
    }

    @DoMeasure
    @Transactional
    @Override
    public List<Double> findAvgByUserId() {
        return incomeRepository.findAvgByUserId();
    }

    @Cacheable(cacheNames = "incomeByUserId")
    public List<Income> findFromCacheByUserIdIs(Integer id) {
        List<Income> incomes = findByUserIdIs(id);
        return incomes;
    }

    @Cacheable(cacheNames = "incomeAvg")
    public List<Double> findFromCacheAvgByUserId() {
        List<Double> avg = findAvgByUserId();
        return avg;
    }

    @CachePut(cacheNames = "incomeByUserId")
    public List<Income> findFromCacheByUserIdIsAndUpdate(Integer id) {
        List<Income> incomes = findByUserIdIs(id);
        return incomes;
    }

    @CachePut(cacheNames = "incomeAvg")
    public List<Double> findFromCacheAvgByUserIdAndUpdate() {
        List<Double> avg = findAvgByUserId();
        return avg;
    }
}
