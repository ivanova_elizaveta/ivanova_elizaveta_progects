package com.SpringBootSecurity.service;

import com.SpringBootSecurity.DoMeasure;
import com.SpringBootSecurity.Repository.RoleRepository;
import com.SpringBootSecurity.Repository.UserRepository;
import com.SpringBootSecurity.model.Role;
import com.SpringBootSecurity.model.User;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
@Profile("!TEST")
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @DoMeasure
    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

    public User findUserById(Long userId) {
        Optional<User> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new User());
    }

    @DoMeasure
    public List<User> allUsers() {
        List<String> list = userRepository.findAll().stream().map(u -> u.getLogin()).distinct().filter(a -> !a.isEmpty() && a != null).sorted().collect(Collectors.toList());
        List<User> u = userRepository.findAll().stream().filter(a -> a.getLogin() == null).peek(a -> a.setLogin("default")).collect(Collectors.toList());
        return userRepository.findAll();
    }

    @DoMeasure
    public boolean saveUser(User user) {
        User userFromDB = userRepository.findByLogin(user.getUsername());

        if (userFromDB != null) {
            return false;
        }

        user.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));
        user.setPassword(user.getPassword());
        userRepository.save(user);
        return true;
    }

    @Cacheable(cacheNames = "userByName")
    public User loadFromCacheByUsername(String name) {
        User user = loadUserByUsername(name);
        return user;
    }

    @Cacheable(cacheNames = "userById")
    public User findFromCacheById(Long userId) {
        User user = findUserById(userId);
        return user;
    }

    @Cacheable(cacheNames = "allUsers")
    public List<User> allUsersFromCache() {
        List<User> users = allUsers();
        return users;
    }

    @CachePut(cacheNames = "userByName")
    public User loadFromCacheByUsernameAndUpdate(String name) {
        User user = loadUserByUsername(name);
        return user;
    }

    @CachePut(cacheNames = "userById")
    public User findFromCacheByIdAndUpdate(Long userId) {
        User user = findUserById(userId);
        return user;
    }

    @CachePut(cacheNames = "allUsers")
    public List<User> allUsersFromCacheAndUpdate() {
        List<User> users = allUsers();
        return users;
    }

}



