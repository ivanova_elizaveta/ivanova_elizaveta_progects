package com.SpringBootSecurity.service;

import com.SpringBootSecurity.Repository.RoleRepository;
import com.SpringBootSecurity.Repository.UserRepository;
import com.SpringBootSecurity.model.User;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


@Service
@NoArgsConstructor
@Profile("TEST")
public class TestUserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;

    public TestUserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    String homeDir = System.getProperty("user.home");
    String file = homeDir + File.separator + "Test.txt";
    Scanner scanner;

    {
        try {
            scanner = new Scanner(new File(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User findUserById(Long userId) {
        User user = new User();
        while (scanner.hasNext()) {
            if (userId.equals(Long.parseLong(scanner.nextLine()))) {
                user.setId(Long.parseLong(scanner.nextLine()));
                user.setLogin(scanner.nextLine());
                user.setPassword(scanner.nextLine());
            }
        }
        return user;
    }

    @Override
    public List<User> allUsers() {
        List<User> users = new ArrayList<>();
        if (scanner.hasNext()) {
            User user = new User();
            user.setId(Long.parseLong(scanner.nextLine()));
            user.setLogin(scanner.nextLine());
            user.setPassword(scanner.nextLine());
            users.add(user);
        }
        return users;
    }

    @Override
    public boolean saveUser(User user) {
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(user.getId().toString());
            writer.write(user.getLogin());
            writer.write(user.getPassword());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
