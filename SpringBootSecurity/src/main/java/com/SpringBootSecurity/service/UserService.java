package com.SpringBootSecurity.service;

import com.SpringBootSecurity.model.User;

import java.util.List;

public interface UserService {

    User findUserById(Long userId);

    List<User> allUsers();

    public boolean saveUser(User user);


}
