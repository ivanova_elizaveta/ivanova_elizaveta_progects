package com.SpringBootSecurity.service;

import com.SpringBootSecurity.model.Income;

import java.util.List;

public interface IncomeService {
    List<Income> findByUserIdIs(Integer id);

    List<Double> findAvgByUserId();

    Income addIncome(Income income);

}
