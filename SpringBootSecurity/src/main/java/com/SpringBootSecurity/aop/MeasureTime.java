//package com.SpringBootSecurity.aop;
//
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//public class MeasureTime {
//
//    private long minTime = Long.MAX_VALUE;
//    private long maxTime = Long.MIN_VALUE;
//
//    @Pointcut("@annotation(com.SpringBootSecurity.DoMeasure)")
//    public void loggableMethod() { }
//
//    @Around("loggableMethod()")
//    public Object measuring(ProceedingJoinPoint joinPoint ) {
//        long start = System.currentTimeMillis();
//
//        Object output = null;
//
//        try {
//            output = joinPoint.proceed();
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
//
//        long end = System.currentTimeMillis();
//        long time = end - start;
//
//        if (time > maxTime) maxTime = time;
//        if (time < minTime) minTime = time;
//
//        System.out.println("Time: "+ maxTime + " " + minTime);
//
//        return output;
//    }
//}
