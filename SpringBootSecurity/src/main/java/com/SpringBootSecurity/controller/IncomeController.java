package com.SpringBootSecurity.controller;

import com.SpringBootSecurity.DoMeasure;
import com.SpringBootSecurity.Repository.IncomeRepository;
import com.SpringBootSecurity.model.Income;
import com.SpringBootSecurity.service.IncomeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestMapping(value = "/income")
@RestController
public class IncomeController {

    @Resource(name = "incomeRepository")
    private final IncomeRepository incomeRepository;

    private final IncomeServiceImpl incomeService;

    @Autowired
    public IncomeController(IncomeRepository incomeRepository, IncomeServiceImpl incomeService) {
        this.incomeRepository = incomeRepository;
        this.incomeService=incomeService;
    }

    @DoMeasure
    @GetMapping
    @ResponseBody
    public List<Income> showIncome(Integer id) {
        List<Income> listIncome = incomeService.findFromCacheByUserIdIs(id);
        for (Income i : listIncome) {
            listIncome.add(i);
        }
        return listIncome;
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    @Consumes(APPLICATION_JSON)
    public Income addIncome(@Valid @RequestBody Income income) {
        Income inc = incomeRepository.save(income);
        return inc;
    }
}
