package com.SpringBootSecurity.controller;

import com.SpringBootSecurity.Repository.UserRepository;
import com.SpringBootSecurity.model.User;
import com.SpringBootSecurity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RestController
@Validated
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;


    @GetMapping(value = "/user")
    public List<User> showUser() {
        List<User> users = userService.allUsers();
        return users;
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    @Consumes(APPLICATION_JSON)
    public boolean addUser(@Valid @RequestBody User user) {
        return userService.saveUser(user);

    }
}
