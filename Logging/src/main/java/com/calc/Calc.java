package com.calc;


import com.calc.Operation.Operation;
import com.calc.Validator;


import java.util.Iterator;

public abstract class Calc implements Iterable<Operation> {
    private String name;
    private Operation[] operations;
    private Validator[] validators;


    public Iterator<Operation> iterator() {
        return new Iterator<Operation>() {
            private int currentIndex = 0;


            public boolean hasNext() {
                return currentIndex < operations.length;
            }


            public Operation next() {
                if (!hasNext()) {
                    throw new IndexOutOfBoundsException("...");
                }
                return operations[currentIndex++];
            }

            public void remove() {

            }
        };
    }

    public Calc(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || "".equals(name)) {
            throw new IllegalArgumentException("Calc name is undefined");
        }
        this.name = name;
    }

    public Operation[] getOperations() {
        return operations;
    }

    public void setOperations(Operation[] operations) throws CalcException {
        if (operations == null || operations.length == 0) {
            throw new CalcException(new IllegalArgumentException("..."));
        }
        this.operations = operations;
    }

    public Validator[] getValidators() {
        return validators;
    }

    public void setValidators(Validator[] validators) throws CalcException {
        if (validators == null || validators.length == 0) {
            throw new CalcException(new IllegalArgumentException("..."));
        }
        this.validators = validators;
    }

    public abstract String calc(String expression);


}
