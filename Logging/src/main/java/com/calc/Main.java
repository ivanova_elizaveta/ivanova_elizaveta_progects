package com.calc;

import com.calc.Display.Display;
import com.calc.Display.SimpleConsoleDisplay;
import com.calc.Display.WebDisplay;
import com.calc.Operation.Multiply;
import com.calc.Operation.Operation;
import com.calc.Operation.Pi;
import com.calc.Operation.Plus;

import java.util.Iterator;
import org.apache.log4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final String result = "6";
    private static String params = "2+2*2";


    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class.getName());

    public static void main(String[] args) throws CalcException {
        final Display display = createDisplay("SIMPLE_CONSOLE");
        final Operation[] operations = initOperation();
        final Validator validator = new SimpleCalcValidator();
        validator.setOperations(operations);
        final Calc calc = new JSCalc();

        LOGGER.info("Program start!");


        Iterator<Operation> iterator = calc.iterator();
        for (Operation o : operations) {
            System.out.println(o.getOperationPattern());
            System.out.println(o.getPriority());
        }

        calc.setOperations(operations);
        calc.setValidators(new Validator[]{validator});
        display.show(result.equals(calc.calc(params)) ? "OK" : "Fail");

        LOGGER.info("Program finish!");
    }

    private static Operation[] initOperation() {
        return new Operation[]{
                new Plus(),
                new Multiply(),
                new Pi()
        };
    }


    private static Display createDisplay(String param) throws CalcException {
        if (param == null) {
            throw new CalcException(new IllegalArgumentException("..."));
        }
        Display result;

        switch (param) {
            case "WEB":
                result = new WebDisplay();
                break;
            case "SIMPLE_CONSOLE":
                result = new SimpleConsoleDisplay();
                break;
            default:
                throw new CalcException(new IllegalArgumentException("..."));
        }
        return result;
    }


}
