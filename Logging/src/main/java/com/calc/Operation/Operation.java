package com.calc.Operation;

public interface Operation {
    String getOperationPattern();

    /**
     * http://www.cs.bilkent.edu.tr/~guvenir/courses/CS101/op_precedence.html
     *
     * @return Operation.Operation priority
     */
    int getPriority();

    /**
     * Ассоциативность
     * https://ru.wikipedia.org/wiki/%D0%90%D1%81%D1%81%D0%BE%D1%86%D0%B8%D0%B0%D1%82%D0%B8%D0%B2%D0%BD%D0%B0%D1%8F_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D1%8F
     *
     * @return True if the operation is associative
     */
    boolean isAssociative();

    /**
     * Коммутативность
     * https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BC%D0%BC%D1%83%D1%82%D0%B0%D1%82%D0%B8%D0%B2%D0%BD%D0%B0%D1%8F_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D1%8F
     *
     * @return true if operation is commutative
     */
    boolean isCommutative();

    boolean canEval(String expression);

    String eval(String expression);

}
