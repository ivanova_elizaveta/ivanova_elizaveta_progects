package com.calc.Operation;
import com.calc.Operation.Operation;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Multiply implements Operation {
    private static final Pattern PATTERN_GLOBAL = Pattern.compile(
            "\\d+\\.?\\d+\\s*[*xX]?\\s*\\d+\\.?\\d+",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

    public String getOperationPattern() {
        return PATTERN_GLOBAL.pattern();
    }


    public int getPriority() {
        return 12;
    }


    public boolean isAssociative() {
        return true;
    }


    public boolean isCommutative() {
        return true;
    }


    public boolean canEval(String expression) {
        return PATTERN_GLOBAL.matcher(expression).find();
    }


    public String eval(String expression) {
        final StringBuffer stringBuffer = new StringBuffer(expression.length());
        final Matcher matcher = PATTERN_GLOBAL.matcher(expression);
        while (matcher.find()) {
            String currentExpression = expression.substring(matcher.start(), matcher.end());
            currentExpression = currentExpression.toLowerCase();
            String operandsSplitter = "";
            if (currentExpression.contains("*")) {
                operandsSplitter = "\\*";
            } else {
                if (currentExpression.contains("x")) {
                    operandsSplitter = "x";
                }
            }
            final String[] operands = currentExpression.split(operandsSplitter);
            final BigDecimal leftOperand = new BigDecimal(operands[0].trim());
            final BigDecimal rightOperand = new BigDecimal(operands[1].trim());

            final BigDecimal result = leftOperand.multiply(rightOperand);
            matcher.appendReplacement(stringBuffer, result.toString());
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }


}
