package com.calc.Operation;

import com.calc.Operation.Operation;

public class Plus implements Operation {

    public String getOperationPattern() {
        return "+";
    }


    public int getPriority() {
        return 11;
    }


    public boolean isAssociative() {
        return true;
    }


    public boolean isCommutative() {
        return true;
    }


    public boolean canEval(String expression) {
        return expression.contains(getOperationPattern());
    }


    public String eval(String expression) {
        String[] operands = expression.split(getOperationPattern());
        String result = "";
        // 2 + 2 + 2 * 4
        // 4 + 2 * 4
        // 4 + 8
        for (String operand : operands) {
            result = String.valueOf(
                    Double.parseDouble(result) +
                            Double.parseDouble(operand));
        }
        return result;
    }

}
