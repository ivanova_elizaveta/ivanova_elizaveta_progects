package com.calc;

import com.calc.Calc;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;


public class JSCalc extends Calc {
    private static final ScriptEngineManager SCRIPT_ENGINE_MANAGER = new ScriptEngineManager();
    private static final ScriptEngine ENGINE = SCRIPT_ENGINE_MANAGER.getEngineByName("js");

    public JSCalc() {
        super("Js calc");
    }

    @Override
    public String calc(String expression) {
        String result = "";
        try {
            result = ENGINE.eval(expression).toString();
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        return result;
    }

}
