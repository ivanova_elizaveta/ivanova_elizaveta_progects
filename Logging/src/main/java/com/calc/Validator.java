package com.calc;


import com.calc.Operation.Operation;


public abstract class Validator {

    protected Operation[] operations;

    public void setOperations(Operation[] operations) {
        this.operations = operations;
    }

    public abstract boolean isValid(String expression);

    public void validate(String expression) throws CalcException {
        if (!isValid(expression)) {
            throw new CalcException("Invalid expression");
        }
    }

}
