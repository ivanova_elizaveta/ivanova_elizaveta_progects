package com.calc;


import com.calc.Calc;
import com.calc.Operation.Operation;


public class SimpleCalc extends Calc {
    public SimpleCalc() {
        super("Simple calc");
    }

    @Override
    public String calc(String expression) {
        String result = expression;
        for (Operation operation : getOperations()) {
            result = operation.eval(result);
        }
        return result;
    }

}
