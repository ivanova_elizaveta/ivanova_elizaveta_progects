import java.io.IOException;


public class Main {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
        String homeDir = System.getProperty("user.home");

        ClassLoader loader;
        loader = new MyClassLoader(homeDir);
        Class clazz1 = Class.forName("TestClass1", true, loader);

        Modul m1 = (Modul) clazz1.newInstance();
        m1.test();

        Class clazz2 = Class.forName("TestClass2", true, loader);

        Modul m2 = (Modul) clazz2.newInstance();
        m2.test();

    }
}
