public class Div implements Operation {
    @Override
    public String execute(int num1, int num2) {
        String res = "";
        try {
            res = Integer.toString(num1 / num2);
        } catch (ArithmeticException e) {
            System.out.println("Division By Zero");
        }
        return res;
    }

    @Override
    public String execute(double num1, double num2) {
        String res = "";
        try {
            res = Double.toString(num1 / num2);
        } catch (ArithmeticException e) {
            System.out.println("Division By Zero");
        }
        return res;
    }
}
