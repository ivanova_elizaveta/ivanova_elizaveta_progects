public class Sum implements Operation {


    @Override
    public String execute(int num1, int num2) {
        return Integer.toString(num1 + num2);
    }

    @Override
    public String execute(double num1, double num2) {
        return Double.toString(num1 + num2);
    }
}
