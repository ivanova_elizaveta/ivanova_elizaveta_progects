public interface Operation {

    String execute(int num1, int num2);

    String execute(double num1, double num2);




}
