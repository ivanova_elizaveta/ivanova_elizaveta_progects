public class Init implements Calc {

    private final char SPLIT = '.';
    private String params = "";
    public int operCount = 0;
    public String[] numbers = new String[10];
    public String[] operations = new String[10];


    public Init(String params) {
        this.params = params;
        String current = new String();
        int index1 = 0;
        int index2 = 0;
        int i = 0;
        int j = 0;
        for (char symb : params.toCharArray()) {
            if (Character.isDigit(symb) || symb == SPLIT) {
                current += symb;
                this.numbers[j] = current;
                i++;
            } else {
                this.numbers[index1++] = current;
                j++;
                current = "";
                this.operations[index2++] = Character.toString(symb);
                operCount++;
            }
        }
    }

    public void setOperCount(int operCount) {
        this.operCount = operCount;
    }


    @Override
    public String exec() {
        int j = 0;
        String res = numbers[j];
        for (int i = 0; i < operCount; i++) {
            String num = numbers[j + 1];
            Operation operation = null;

            char n = operations[i].charAt(0);
            switch (n) {
                case '+':
                    operation = new Sum();
                    break;
                case '-':
                    operation = new Sub();
                    break;
                case '*':
                    operation = new Mul();
                    break;
                case '/':
                    operation = new Div();
                    break;
            }
            if (res.contains(Character.toString(SPLIT)) || this.numbers[j + 1].contains(Character.toString(SPLIT))) {
                res = operation.execute(Double.parseDouble(res), Double.parseDouble(num));
            } else {
                res = operation.execute(Integer.parseInt(res), Integer.parseInt(num));
            }
            j++;

        }

        return res;
    }


}
