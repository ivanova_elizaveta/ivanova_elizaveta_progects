public class ChickenVoice {
    static EggVoice egg;

    public static void main(String[] args) {
        egg = new EggVoice();
        System.out.println("Спор начат...");

        try {
            egg.start();
            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                System.out.println("курица!");
            }
            if (egg.isAlive()) {
                try {
                    egg.join();
                } catch (InterruptedException e) {
                }
                System.out.println("Первым появилось яйцо!");
            } else {
                System.out.println("Первой появилась курица!");
            }
            System.out.println("Спор закончен!");
        } catch (NullPointerException e) {
            System.out.println("egg is null");
        }
    }


}
