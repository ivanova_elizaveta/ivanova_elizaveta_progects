public class Program02 {

    public static void main(String[] args) {
        Thread myThready = new Thread(() -> System.out.println("Привет из побочного потока!"));
        myThready.start();
        System.out.println("Главный поток завершён...");
    }

}

