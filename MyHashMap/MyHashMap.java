import java.util.*;

public class MyHashMap<K, V> implements Map<K, V> {
    private class MyEntry implements Entry<K, V> {
        K key;
        V value;

        public MyEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            return this.value = value;
        }
    }


    private final List<List<MyEntry>> buckets = new ArrayList<>();
    private static final int BUCKET_COUNT = 10;
    private int count = 10;
    private static final int SIZE_INT = 2147483647;
    private int cnt = 0;
    private int bucketCnt = BUCKET_COUNT;

    public MyHashMap(int bucketCount) {
        for (int i = 0; i <= bucketCount; i++) {
            buckets.add(new ArrayList<>());
        }
        bucketCnt = bucketCount;
        count = bucketCount;
    }

    public MyHashMap() {
        this(BUCKET_COUNT);
    }

    private int getBucketId(int hashCode) {
        int buckedId = 0;
        int cntItemInBucket = (int) Math.floor(SIZE_INT / bucketCnt);
        for (int i = 1; i <= bucketCnt; i++) {
            if (Math.abs(hashCode) < cntItemInBucket * i) {
                buckedId = i - 1;
                break;
            }
        }
        return buckedId;
    }


    @Override
    public int size() {
        return cnt;
    }

    @Override
    public boolean isEmpty() {
        return cnt == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        boolean hasKey = false;
        int hashCodeKey = key.hashCode();
        int buсketId = getBucketId(hashCodeKey);
        for (int j = 0; j < buckets.get(buсketId).size(); j++) {
            MyEntry myEntry = buckets.get(buсketId).get(j);
            if (myEntry.getKey() == key) {
                hasKey = true;
                break;
            }
        }
        return hasKey;

    }


    @Override
    public boolean containsValue(Object value) {
        boolean hasValue = false;
        for (int i = 0; i < buckets.size(); i++) {
            for (int j = 0; j < buckets.get(i).size(); j++) {
                MyEntry myEntry = buckets.get(i).get(j);
                if (myEntry.getValue() == value) {
                    hasValue = true;
                    break;
                }
            }
        }
        return hasValue;

    }

    @Override
    public V get(Object key) {
        V value = null;
        int hashCodeKey = key.hashCode();
        int buсketId = getBucketId(hashCodeKey);
        for (int j = 0; j < buckets.get(buсketId).size(); j++) {
            MyEntry myEntry = buckets.get(buсketId).get(j);
            if (myEntry.getKey() == key) {
                value = myEntry.getValue();
                break;
            }
        }
        return value;

    }

    @Override
    public Object put(Object k, Object v) {
        K key = (K) k;
        V value = (V) v;
        if (!containsKey(k)) {
            int hashCodeKey = key.hashCode();
            int bucketId = getBucketId(hashCodeKey);
            MyEntry myEntry = new MyEntry(key, value);
            buckets.get(bucketId).add(myEntry);
            cnt++;
        }
        return buckets;
    }


    @Override
    public V remove(Object key) {

        int hashCodeKey = key.hashCode();
        int bucketId = getBucketId(hashCodeKey);
        V value = null;
        if (containsKey(key)) {
            for (int j = 0; j < buckets.get(bucketId).size(); j++) {
                MyEntry myEntry = buckets.get(bucketId).get(j);
                if (myEntry.getKey().equals(key)) {
                    buckets.get(bucketId).remove(myEntry);
                    value = myEntry.getValue();
                    cnt--;
                    break;
                }
            }
        }
        return value;

    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        Set set = m.keySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            K keys = (K) iterator.next();
            if (!containsKey(keys)) {
                put(keys, m.get(keys));
            }
        }

    }

    @Override
    public void clear() {
        cnt = 0;

    }

    @Override
    public Set<K> keySet() {

        Set<K> set = new HashSet<>();
        for (int i = 0; i < buckets.size(); i++) {
            for (int j = 0; j < buckets.get(i).size(); j++) {
                MyEntry myEntry = buckets.get(i).get(j);
                if (!set.contains(myEntry.getKey())) {
                    set.add(myEntry.getKey());
                }
            }
        }
        return set;

    }

    @Override
    public Collection<V> values() {

        Collection<V> collection = new ArrayList<>();
        for (int i = 0; i < buckets.size(); i++) {
            for (int j = 0; j < buckets.get(i).size(); j++) {
                MyEntry myEntry = buckets.get(i).get(j);
                collection.add(myEntry.getValue());
            }
        }
        return collection;

    }

    @Override
    public Set<Entry<K, V>> entrySet() {

        Set<Entry<K, V>> entries = new HashSet<>();
        for (int i = 0; i < buckets.size(); i++) {
            for (int j = 0; j < buckets.get(i).size(); j++) {
                MyEntry myEntry = buckets.get(i).get(j);
                entries.add(myEntry);
            }
        }
        return entries;

    }
}
