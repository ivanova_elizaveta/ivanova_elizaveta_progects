public class Main {
    public static void main(String[] args) {

        MyHashMap<Integer, String> defaultHashMap = new MyHashMap<>();

        System.out.println("size: " + defaultHashMap.size());
        System.out.println("empty: " + defaultHashMap.isEmpty());

        defaultHashMap.put(1, "first");
        defaultHashMap.put(2, "second");
        defaultHashMap.put(3, "third");
        defaultHashMap.put(4, "fourth");

        System.out.println("size: " + defaultHashMap.size());
        System.out.println("get: " + defaultHashMap.get(1));
        System.out.println("remove: " + defaultHashMap.remove(2));
        System.out.println("size: " + defaultHashMap.size());
        System.out.println("containsKey: " + defaultHashMap.containsKey(3));
        System.out.println("containsValue: " + defaultHashMap.containsValue("second"));
        System.out.println("keySet: " + defaultHashMap.keySet().toString());
        System.out.println("values: " + defaultHashMap.values().toString());
        System.out.println("putAll : ");
        System.out.println("clear: ");
        defaultHashMap.clear();
        System.out.println("size: " + defaultHashMap.size());


    }
}
