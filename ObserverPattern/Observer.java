public interface Observer {
    void update(double speed, double engineSpeed, int oilPressure);
}
