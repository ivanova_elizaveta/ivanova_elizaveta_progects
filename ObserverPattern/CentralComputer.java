import java.util.ArrayList;
import java.util.List;

public class CentralComputer implements Observable {
    private List<Observer> observers = new ArrayList<Observer>();
    private double speed;
    private double engineSpeed;
    private int oilPressure;

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer o : observers) {
            o.update(speed, engineSpeed, oilPressure);
        }
    }

    public void changeData(double speed, double engineSpeed, int oilPressure) {
        this.speed = speed;
        this.engineSpeed = engineSpeed;
        this.oilPressure = oilPressure;
        notifyObservers();
    }
}
