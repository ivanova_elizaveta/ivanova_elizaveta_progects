public class CurrentDataDisplay implements Observer {
    private double speed;
    private double engineSpeed;
    private int oilPressure;

    public CentralComputer centralComputer;

    public CurrentDataDisplay(CentralComputer centralComputer) {
        this.centralComputer = centralComputer;
        centralComputer.registerObserver(this);
    }

    @Override
    public void update(double speed, double engineSpeed, int oilPressure) {
        this.speed = speed;
        this.engineSpeed = engineSpeed;
        this.oilPressure = oilPressure;
        display();
    }

    public void display() {
        System.out.print("Speed: " + speed + "\nEngine Speed: " + engineSpeed + "\nOil Pressure: " + oilPressure + "\n");
    }
}
