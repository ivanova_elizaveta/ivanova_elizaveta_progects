public class SecondThread extends Thread {
    SecondThread(String name) {
        super(name);
    }

    int i = 0;

    @Override
    public void run() {

        while (i < 10) {
            System.out.println(Thread.currentThread().getName() + " i =" + i);
            Thread.yield();
            i++;
        }
        System.out.println("Ending of " + Thread.currentThread().getName());
    }
}
