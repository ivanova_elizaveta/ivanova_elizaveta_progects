public class ThirdThread extends Thread {
    ThirdThread(String name) {
        super(name);
    }

    @Override
    public void run() {

        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + "is running");
        }
        System.out.println("End of third thread");

    }
}
