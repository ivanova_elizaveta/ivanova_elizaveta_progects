public class FirstThread extends Thread {

    FirstThread(String name) {
        super(name);
    }


    @Override
    public void run() {
        System.out.println("\nStart of the " + Thread.currentThread().getName());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Interruption of thread");
        }
        System.out.println("Ending of the " + Thread.currentThread().getName());
    }
}
