public class Program {
    public static int mValue = 0;
    static Incremenator mInc;
    static FirstThread firstThread;
    static SecondThread secondThread;
    static ThirdThread thirdThread;

    public static void main(String[] args) throws InterruptedException {
        mInc = new Incremenator();
        firstThread = new FirstThread("firstThread");
        secondThread = new SecondThread("secondThread");
        thirdThread = new ThirdThread("thirdThread");

        mInc.start();

        for (int i = 1; i <= 3; i++) {
            try {
                Thread.sleep(i * 2 * 1000);
            } catch (InterruptedException e) {
            }
            mInc.changeAction();
        }
        mInc.interrupt();

        firstThread.start();
        firstThread.join();
        secondThread.start();
        secondThread.join();
        thirdThread.start();
        thirdThread.join();


        System.out.println("\nEnd of main thread");

    }

}
