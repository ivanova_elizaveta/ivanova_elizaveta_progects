public class Cook {
    public String getSushi() {
        return "Sushi";
    }

    public String getSteak() {
        return "Steak";
    }

    public String getSalad() {
        return "Salad";
    }
}
