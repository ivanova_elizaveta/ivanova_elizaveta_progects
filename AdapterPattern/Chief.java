public interface Chief {
    public Object seaFood();

    public Object meatFood();

    public Object veganFood();

}
