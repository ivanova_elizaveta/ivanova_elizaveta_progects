public class CarIsNotReadyExeption extends Exception {
    public CarIsNotReadyExeption(String message) {
        super(message);
    }
}
