public class Main {
    public static void main(String[] args) {

        Car car = new Car("Toyota");
        car.fillTank();
        car.startCar();
        try {
            car.ride();

        } catch (CarIsNotReadyExeption e) {
            System.out.println(e.getMessage());
            System.out.println("Бак наполнен? " + car.fillTank + "\nАвтомобиль заведен? " + car.startCar);
        } finally {
            System.out.println("Работа программы окончена.");
        }
    }


}
