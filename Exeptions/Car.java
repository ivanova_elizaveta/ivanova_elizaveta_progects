public class Car {
    private String name;
    boolean fillTank;
    boolean startCar;


    public Car(String name) {
        this.name = name;
    }

    public void fillTank() {
        System.out.println("Бак наполнен!");
        this.fillTank = true;
    }

    public void startCar() {
        System.out.println("Автомобиль заведен!");
        this.startCar = true;
    }

    public void ride() throws CarIsNotReadyExeption {
        System.out.println("Собираемся выезжать!");
        if (fillTank && startCar) {
            System.out.println("Машина готова к поездке");
        } else {
            throw new CarIsNotReadyExeption("Машина не готова к поездке!");

        }
    }
}
