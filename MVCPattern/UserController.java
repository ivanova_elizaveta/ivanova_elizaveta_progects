public class UserController {
    private UserModel um;
    private View view;


    public UserController(UserModel um, View view) {
        this.um = um;
        this.view = view;
    }

    public UserModel getUm() {
        return um;
    }

    public void setUm(UserModel um) {
        this.um = um;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public void modelView() {
        view.print(um);
    }
}
