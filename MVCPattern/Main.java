public class Main {
    public static void main(String[] args) {
        UserModel userModel = new UserModel();
        View view = new View();
        UserController controller = new UserController(userModel, view);

        userModel.setfName("Ivan");
        userModel.setlName("Ivanov");
        userModel.setAge(32);
        view.print(userModel);

        UserModel userModel2 = new UserModel();
        userModel2.setfName("Petr");
        userModel2.setlName("Petrov");
        userModel2.setAge(45);

        controller.setUm(userModel2);
        controller.modelView();


    }
}
