public class Main {
    public static void main(String[] args) {

        Circle circle = new Circle(10, 20, 15, "red");
        Rectangle rectangle = new Rectangle(15, 25, "blue", 10, 10);
        try {
            Circle circle1 = (Circle) circle.clone();
            System.out.println(circle.color);
            System.out.println(circle1.color);
            System.out.println(circle1.radius);

            Rectangle rectangle1 = (Rectangle) rectangle.clone();
            System.out.println(rectangle.height);
            System.out.println(rectangle1.height);
            System.out.println(rectangle1.color);

        } catch (CloneNotSupportedException e) {
            return;
        }

    }
}
