public class Main {
    public static void main(String[] args) {
        CentralComputer c = new CentralComputer();
        CurrentDataDisplay d = new CurrentDataDisplay(c);

        c.changeData(34.4, 23.6, 54);
        c.changeData(86.5, 33.09, 456);

    }
}
