public class CakeFactory {

    public enum CakeTypes {
        CHERRY,
        CHOCOLATE
    }

    public Cake getCake(CakeTypes t) {
        Cake result = null;
        switch (t) {
            case CHERRY:
                result = new CherryCake();
                break;
            case CHOCOLATE:
                result = new ChocolateCake();
                break;
            default:
                throw new IllegalArgumentException("Wrong type" + t);
        }
        return result;
    }
}
