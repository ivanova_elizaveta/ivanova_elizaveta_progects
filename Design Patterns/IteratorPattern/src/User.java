public class User implements Container {
    public static final String[] users = {"User1", "User2", "User3", "User4", "User5"};


    @Override
    public Iterator getIterator() {
        return new UserIterator();
    }

    private class UserIterator implements Iterator {

        private int count;

        @Override
        public boolean hasNext() {
            return count < users.length;
        }

        @Override
        public Object next() {
            if (this.hasNext()) {
                return users[count++];
            }
            return null;
        }
    }
}

