public class Rectangle extends Shape {

    public int width;
    public int height;

    public Rectangle() {
    }

    public Rectangle(int x, int y, String color, int height, int width) {
        super(x, y, color);
        this.width = width;
        this.height = height;
    }


}
