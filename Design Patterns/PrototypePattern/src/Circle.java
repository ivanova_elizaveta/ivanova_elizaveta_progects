public class Circle extends Shape {
    public int radius;

    public Circle() {
    }

    public Circle(int x, int y, int rad, String color) {
        super(x, y, color);
        this.radius = rad;
    }


}
