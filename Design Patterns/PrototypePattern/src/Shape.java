public class Shape implements Cloneable {
    protected int x;
    protected int y;
    protected String color;

    public Shape() {
    }

    public Shape(int x, int y, String color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public Shape clone() throws CloneNotSupportedException {
        return (Shape) super.clone();
    }


}



