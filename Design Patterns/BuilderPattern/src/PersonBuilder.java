public abstract class PersonBuilder {
    protected Person person;

    public Person getPerson() {
        return person;
    }

    public void createNewPerson() {
        person = new Person();
    }

    public abstract void createFName();

    public abstract void createLName();

    public abstract void createSalary();

}
