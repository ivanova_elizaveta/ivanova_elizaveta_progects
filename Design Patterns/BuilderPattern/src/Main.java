public class Main {
    public static void main(String[] args) {
        HR hr = new HR();

        PersonBuilder lawer = new Lawer();

        hr.setPersonBuilder(lawer);
        hr.construcPerson();

        Person person = hr.getPerson();
        System.out.println(person.getUserName());

        PersonBuilder manager = new Manager();

        hr.setPersonBuilder(manager);
        hr.construcPerson();
        Person person1 = hr.getPerson();
        System.out.println(person1.getUserName());

    }
}
