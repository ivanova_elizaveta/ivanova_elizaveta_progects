public class Client {
    public static void eat(Object dish) {
        System.out.println(dish);
    }

    public static void main(String[] args) {
        Chief c = new ChiefAdapter();
        Object dish = c.seaFood();
        eat(dish);
        dish = c.meatFood();
        eat(dish);
        dish = c.veganFood();
        eat(dish);
    }
}
