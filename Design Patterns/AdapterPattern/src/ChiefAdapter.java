public class ChiefAdapter extends Cook implements Chief {

    @Override
    public Object seaFood() {
        return getSushi();
    }

    @Override
    public Object meatFood() {
        return getSteak();
    }

    @Override
    public Object veganFood() {
        return getSalad();
    }
}
