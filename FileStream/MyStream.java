import java.io.*;

public class MyStream {

    public static void main(String[] args) {
        final String encoding = "UTF-8";
        String homeDir = System.getProperty("user.home");
        System.out.println(homeDir);
        String stringFilePath = homeDir + File.separator + "Files";
        String fileName = "file";

        final File file = new File(stringFilePath + File.separator + fileName);
        int[] info = {1, 2, 3, 4, 5};

        try (final FileOutputStream o = new FileOutputStream(file)) {
            for (int n : info) {
                o.write(n);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedInputStream i = new BufferedInputStream(new FileInputStream(file))) {
            while (true) {
                int value = i.read();
                System.out.print(value + ", ");
                if (value == -1) {
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println();

        String str = "Some data";
        char buffer[] = new char[str.length()];
        str.getChars(0, str.length(), buffer, 0);

        try (FileWriter w = new FileWriter(file)) {
            w.write(buffer);

        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileReader r = new FileReader(file)) {
            int index;
            while ((index = r.read()) != -1) {
                System.out.println((char) index);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println();


        String line = " new line \n new line \n";
        try (BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), encoding))) {
            w.write(line);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader read = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding))) {
            while ((line = read.readLine()) != null) {
                System.out.println(line);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
