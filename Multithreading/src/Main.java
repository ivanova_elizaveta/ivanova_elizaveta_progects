public class Main {
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();


        MyThread thread1 = new MyThread("First Thread", counter);
        MyThread thread2 = new MyThread("Second Thread", counter);
        MyThread thread3 = new MyThread("Third Thread", counter);
        MyThread thread4 = new MyThread("Fourth Thread", counter);
        MyThread thread5 = new MyThread("Fifth Thread", counter);

        thread1.start();
        thread1.join();
        thread2.start();
        thread2.join();
        thread3.start();
        thread3.join();
        thread4.start();
        thread4.join();
        thread5.start();
        thread5.join();

    }

}
