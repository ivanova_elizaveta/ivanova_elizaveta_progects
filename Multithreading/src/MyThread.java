public class MyThread extends Thread {

    private String threadName;
    private Counter counter;

    public MyThread(String threadName, Counter counter) {
        this.threadName = threadName;
        this.counter = counter;
    }

    @Override
    public void run() {
        counter.increaseCounter();
        System.out.printf(threadName + ": counter= " + counter.getCounter() + "\n");
    }

}
