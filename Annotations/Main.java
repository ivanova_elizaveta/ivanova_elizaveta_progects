import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException, NoSuchFieldException {
        TestClass test = new TestClass();
        test.pritnStr();

        Class c = test.getClass();
        for (Annotation a : c.getAnnotations()) {
            System.out.println(a.toString());
        }

        Method method = c.getMethod("pritnStr");
        for (Annotation a : method.getAnnotations()) {
            System.out.println(a.toString());
        }

        Field field = c.getDeclaredField("str");
        for (Annotation a : field.getAnnotations()) {
            System.out.println(a.toString());
        }

    }
}
