@ClassAnnotation(version = 3)
public class TestClass {

    @FieldAnnotation(version = 5)
    private String str = "String";

    @MethodAnnotation(version = 24)
    public void pritnStr() {
        System.out.println("str: " + str);
    }

}
