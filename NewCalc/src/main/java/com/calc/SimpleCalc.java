import Operation.Operation;


public class SimpleCalc extends Calc {
    public SimpleCalc() {
        super("Simple calc");
    }

    @Override
    public String calc(String expression) {
        String result = expression;
        for (Operation operation : getOperations()) {
            result = operation.eval(result);
        }
        return result;
    }

}
