import Operation.Operation;

public class SimpleCalcValidator extends Validator {
    @Override
    public boolean isValid(String expression) {
        boolean result = true;
        //TODO: нужнжо доработать
        // Может не посчитать выражение, которое сам калькулятор посчитать может
        for (Operation operation : operations) {
            if (!operation.canEval(expression)) {
                result = false;
                break;
            }
        }
        return result;
    }
}
