package Operation;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Pi implements Operation {
    private static final Pattern PATTERN_GLOBAL = Pattern.compile(
            "[p|P][i|I](\\((\\d*|(\\d+\\.?\\d+))\\))?",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    private static final Pattern PATTERN_DIGIT = Pattern.compile("\\d+\\.?\\d+",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

    @Override
    public String getOperationPattern() {
        return PATTERN_GLOBAL.pattern();
    }

    @Override
    public int getPriority() {
        return 15;
    }

    @Override
    public boolean isAssociative() {
        return true;
    }

    @Override
    public boolean isCommutative() {
        return true;
    }

    @Override
    public boolean canEval(String expression) {
        return PATTERN_GLOBAL.matcher(expression).find();
    }

    @Override
    public String eval(String expression) {
        final StringBuffer stringBuffer = new StringBuffer(expression.length());
        final Matcher matcher = PATTERN_GLOBAL.matcher(expression);
        while (matcher.find()) {
            final String currentExpression = expression.substring(matcher.start(), matcher.end());
            final double diameter = getDiameterFromExpression(currentExpression);
            final double piByDiameter = getPI(diameter);
            final BigDecimal pi = BigDecimal.valueOf(piByDiameter);
            matcher.appendReplacement(stringBuffer, pi.toString());
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }

    private static double getDiameterFromExpression(String expression) {
        double diameter = 1;
        final Matcher numberMatcher = PATTERN_DIGIT.matcher(expression);
        if (numberMatcher.find()) {
            final String numberExpression = expression.substring(numberMatcher.start(), numberMatcher.end());
            diameter = Double.parseDouble(numberExpression);
        }
        return diameter;
    }

    private static double getPI(double diameter) {
        final int precision = 1_000_000;
        double pi = 0;
        double d = diameter;
        for (int i = 1; i < precision; i += 2) {
            pi += d * 4 / i;
            d *= -1;
        }
        return pi;
    }

}
