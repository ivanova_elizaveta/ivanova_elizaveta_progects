package Operation;

public class Plus implements Operation {
    @Override
    public String getOperationPattern() {
        return "+";
    }

    @Override
    public int getPriority() {
        return 11;
    }

    @Override
    public boolean isAssociative() {
        return true;
    }

    @Override
    public boolean isCommutative() {
        return true;
    }

    @Override
    public boolean canEval(String expression) {
        return expression.contains(getOperationPattern());
    }

    @Override
    public String eval(String expression) {
        String[] operands = expression.split(getOperationPattern());
        String result = "";
        // 2 + 2 + 2 * 4
        // 4 + 2 * 4
        // 4 + 8
        for (String operand : operands) {
            result = String.valueOf(
                    Double.parseDouble(result) +
                            Double.parseDouble(operand));
        }
        return result;
    }

}
