import Display.Display;
//import Operation.CalcException;
import Operation.Operation;
import Display.SimpleConsoleDisplay;
import Display.WebDisplay;
import Operation.Multiply;
import Operation.Plus;
import Operation.Pi;


import java.util.Iterator;

public class Main {
        private static String params = "2+2*2";
        private static final String result = "6";

        public static void main(String[] args) throws CalcException {
            final Display display = createDisplay("SIMPLE_CONSOLE");
            final Operation[] operations = initOperation();
            final Validator validator = new SimpleCalcValidator();
            validator.setOperations(operations);
            final Calc calc = new JSCalc();

            Iterator<Operation> iterator = calc.iterator();
            for (Operation o : operations) {
                System.out.println(o.getOperationPattern());
                System.out.println(o.getPriority());
            }

            calc.setOperations(operations);
            calc.setValidators(new Validator[]{validator});
            display.show(result.equals(calc.calc(params)) ? "OK" : "Fail");
        }

        private static Operation[] initOperation() {
            return new Operation[]{
                    new Plus(),
                    new Multiply(),
                    new Pi()
            };
        }

        private static Display createDisplay(String param) throws CalcException {
            if (param == null) {
                throw new CalcException(new IllegalArgumentException("..."));
            }
            Display result;
            switch (param) {
                case "WEB":
                    result = new WebDisplay();
                    break;
                case "SIMPLE_CONSOLE":
                    result = new SimpleConsoleDisplay();
                    break;
                default:
                    throw new CalcException(new IllegalArgumentException("..."));
            }
            return result;
        }

    }
