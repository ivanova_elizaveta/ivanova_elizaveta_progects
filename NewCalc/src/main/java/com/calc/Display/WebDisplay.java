package Display;

public class WebDisplay implements Display {
    @Override
    public void show(String stringToShow) {
        // Show "stringToShow" on the site
    }

    @Override
    public void showError(String stringToShow) {
        System.out.println("ERROR");
        show(stringToShow);
    }

}
