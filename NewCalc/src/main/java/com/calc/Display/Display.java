package Display;

public interface Display {
    void show(String stringToShow);
    void showError(String stringToShow);

}
