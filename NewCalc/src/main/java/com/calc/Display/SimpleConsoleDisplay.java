package Display;

public class SimpleConsoleDisplay implements Display {
    private static final String splitter = "=============";

    @Override
    public void show(String stringToShow) {
        System.out.println(splitter);
        System.out.println(stringToShow);
        System.out.println(splitter);
    }

    @Override
    public void showError(String stringToShow) {
        System.out.println("ERROR");
        show(stringToShow);
    }

}
