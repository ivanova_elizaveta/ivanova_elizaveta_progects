import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;



public class Main {
    public static void main(String[] args) throws IOException {

        String homeDir = System.getProperty("user.home");
        File path = new File(homeDir + File.separator + "Pictures");
        String stringFilePath = homeDir + File.separator + "files";
        String fileName = "image";

        if (path.exists() && path.isDirectory()) {
            FileFilter filter = new MyFileFilter();
            File[] files = path.listFiles(filter);
            System.out.println(Arrays.toString(files));
        }

        File tmpFile = File.createTempFile("tmp_", "_" + fileName);

        if (tmpFile.exists() && tmpFile.isFile() && tmpFile.canWrite()) {
            File newTmpFile = new File(tmpFile.toString() + "01");
            if (tmpFile.renameTo(newTmpFile)) {
                tmpFile.delete();
                tmpFile = newTmpFile;
                tmpFile.deleteOnExit();
            }
        }

        File file = new File(stringFilePath + File.separator + fileName);
        File filePath = new File(stringFilePath);

        if ((filePath.exists() || filePath.mkdirs()) && filePath.canWrite() && filePath.canRead()) {
            if (!file.exists()) {
                file.createNewFile();
            }
            if (file.exists() && file.isFile() && file.canWrite()) {
                File newFile = new File(file.toString() + "01");
                if (file.renameTo(newFile)) {
                    file.delete();
                    file = newFile;
                }
            }
        }

    }
}
