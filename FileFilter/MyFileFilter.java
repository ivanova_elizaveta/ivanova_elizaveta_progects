import java.io.File;
import java.io.FileFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MyFileFilter implements FileFilter {

    Pattern pattern = Pattern.compile("jp[e]?g|JP[E]?G|png|PNG|pdf|PDF$");

    @Override
    public boolean accept(File pathname) {
        boolean result = false;

        if (pathname.isFile()) {
            Matcher matcher = pattern.matcher(pathname.toString());
            result = matcher.find();
        }
        return result;
    }
}
