package com.exempl.SimpleWebApp.repos;


import com.exempl.SimpleWebApp.domen.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
