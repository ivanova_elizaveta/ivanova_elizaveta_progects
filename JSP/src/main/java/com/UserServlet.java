package com;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


public class UserServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(UserServlet.class);

    private static final String FILENAME = "${user.home}/slf4j.log";
    private DBConnection DBConnection = new DBConnection();
    private static Random rand;


    public UserServlet() throws SQLException, ClassNotFoundException {
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> users = DBConnection.getUser();

        for (User u : users) {
            logger.info("Get data from DB: id: {}, loggin: {}, password: {}", u.getId(), u.getLogin(), u.getPassword());
        }

        List<String> sortedLogins = users.stream()
                .map(User::getLogin)
                .filter((p) -> p.matches("^\\D*$"))
                .sorted()
                .collect(Collectors.toList());

        request.setAttribute("logins", sortedLogins);
        for (User u : users) {
            logger.info("Set attribute to JSP: id: {}, loggin: {}, password: {}", u.getId(), u.getLogin(), u.getPassword());
        }
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }


}
