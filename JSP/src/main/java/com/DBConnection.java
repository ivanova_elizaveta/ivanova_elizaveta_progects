package com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBConnection {
    private static final Logger logger = LoggerFactory.getLogger(DBConnection.class);

    private final String url = "jdbc:postgresql://localhost/MyDB";
    private final String req = "SELECT * FROM users";
    private Connection con;
    private ResultSet resultSet;
    private Statement statement;
    private PreparedStatement pst;

    public DBConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        con = DriverManager.getConnection(url, "postgres", "root");
    }

    public List<User> getUser() {
        User user;
        List<User> users = new ArrayList<>();

        try {
            statement = con.createStatement();
            pst = con.prepareStatement(req);
            resultSet = pst.executeQuery();
            logger.info("DB query: {}", req);


            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                users.add(user);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public void setUser(User user) throws SQLException {
        String sql = "INSERT INTO users (id, login, password)" +
                " VALUES (?, ?, ? )";

        PreparedStatement ps = con.prepareStatement(sql);
        ps.setLong(1, user.getId());
        ps.setString(2, user.getLogin());
        ps.setString(3, user.getPassword());
        ps.executeUpdate();
    }
}
