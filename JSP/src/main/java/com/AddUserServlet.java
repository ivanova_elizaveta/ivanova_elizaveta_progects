package com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class AddUserServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(UserServlet.class);
    private DBConnection DBConnection = new DBConnection();

    public AddUserServlet() throws SQLException, ClassNotFoundException {
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Data from request: id- {}, login- {}, password- {}", req.getParameter("id"), req.getParameter("login"), req.getParameter("password"));
        User user = new User();
        user.setId(Integer.parseInt(req.getParameter("id")));
        user.setLogin(req.getParameter("login"));
        user.setPassword(req.getParameter("password"));
        try {
            DBConnection.setUser(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        resp.sendRedirect(req.getContextPath() + "/index");
        req.getRequestDispatcher("/addUser.jsp").forward(req, resp);
        System.out.println("Record Added Successfully");
    }
}
