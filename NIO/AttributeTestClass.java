import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AttributeTestClass {
    public static void main(String[] args) throws IOException {
        String p = System.getProperty("user.home");

        Path target = Paths.get(p + "\\testFile.txt");
        Path file = Files.createFile(target);


        try {

            Object object = Files.getLastModifiedTime(file);
            System.out.println("Last modified time: " + object);

            object = Files.size(file);
            System.out.println("Size: " + object);

            object = Files.isSymbolicLink(file);
            System.out.println("isSymbolicLink: " + object);

            object = Files.isDirectory(file);
            System.out.println("isDirectory: " + object);

            Files.delete(file);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
