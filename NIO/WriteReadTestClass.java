import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class WriteReadTestClass {
    public static void main(String[] args) throws IOException {

        String p = System.getProperty("user.home");

        Path path = Paths.get(p + "\\testFile.txt");
        Path file = Files.createFile(path);

        try (BufferedWriter writer = Files.newBufferedWriter(file, StandardCharsets.UTF_8,
                StandardOpenOption.WRITE)) {
            writer.write("Line 1\n");
            writer.write("Line 2\n");
            writer.write("Line 3\n");
        }

        try {
            List<String> lines = Files.readAllLines(file, StandardCharsets.UTF_8);
            for (String string : lines) {
                System.out.println(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
