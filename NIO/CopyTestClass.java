import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class CopyTestClass {
    public static void main(String[] args) throws IOException {

        String p = System.getProperty("user.home");

        Path path = Paths.get(p + "\\testFile.txt");
        Path file = Files.createFile(path);

        Path source = Paths.get(p + "\\testFile.txt");
        Path target = Paths.get(p + "\\Files\\copiedFile.txt");

        try {
            Files.copy(source, target, REPLACE_EXISTING);
            System.out.println("Source file copied successfully!");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
