import java.io.File;

public class Main {
    public static void main(String[] args) {
        String homeDir = System.getProperty("user.home");
        String fileName = homeDir + File.separator + "userFale.csv";

        System.out.println("Write file");
        CSVWriter.write(fileName);

        System.out.println("\nRead file");
        CSVReader.read(fileName);

    }
}
