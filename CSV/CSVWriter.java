import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVWriter {
    private static final String comaSeparator = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    private static final String header = "id,name,age";

    public static void write(String fileName) {

        FileWriter fileWriter = null;

        User user1 = new User(1, "Ivan", 22);
        User user2 = new User(2, "Petro", 30);
        User user3 = new User(3, "Maria", 17);

        List<User> users = new ArrayList();
        users.add(user1);
        users.add(user2);
        users.add(user3);


        try {
            fileWriter = new FileWriter(fileName);

            fileWriter.append(header.toString());

            fileWriter.append(NEW_LINE_SEPARATOR);

            for (User user : users) {
                fileWriter.append(String.valueOf(user.getId()));
                fileWriter.append(comaSeparator);
                fileWriter.append(user.getName());
                fileWriter.append(comaSeparator);
                fileWriter.append(String.valueOf(user.getAge()));
                fileWriter.append(NEW_LINE_SEPARATOR);
            }

            System.out.println("OK");

        } catch (Exception e) {
            System.out.println("File wasn't created");
            e.printStackTrace();
        } finally {

            try {
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Closing error");
                e.printStackTrace();
            }
        }
    }

}
