import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
    private static final String comaSeparator = ",";

    private static final int userId = 0;
    private static final int userNameId = 1;
    private static final int ageId = 2;

    public static void read(String fileName) {

        BufferedReader fileReader = null;

        try {

            List<User> users = new ArrayList();

            String line;

            fileReader = new BufferedReader(new FileReader(fileName));

            fileReader.readLine();

            while ((line = fileReader.readLine()) != null) {

                String[] str = line.split(comaSeparator);
                if (str.length > 0) {

                    User user = new User(Integer.parseInt(str[userId])
                            , str[userNameId]
                            , Integer.parseInt(str[ageId]));
                    users.add(user);
                }
            }

            for (User user : users) {
                System.out.println(user.toString());
            }
        } catch (Exception e) {
            System.out.println("Can't read the file");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                System.out.println("Closing error");
                e.printStackTrace();
            }
        }
    }

}
