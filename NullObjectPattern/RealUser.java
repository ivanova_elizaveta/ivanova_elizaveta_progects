public class RealUser extends AbstractClass {
    private String fName;
    private String lName;

    public RealUser(String fName, String lName) {
        this.fName = fName;
        this.lName = lName;
    }

    @Override
    public void doSomething() {
        System.out.println(fName + " " + lName);

    }
}
