public class Main {
    public static void main(String[] args) {
        AbstractClass realUser = new RealUser("Ivan", "Ivanov");
        realUser.doSomething();

        AbstractClass nullUser = new NullUser();
        nullUser.doSomething();
    }
}
