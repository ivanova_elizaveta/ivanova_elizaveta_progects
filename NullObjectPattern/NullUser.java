public class NullUser extends AbstractClass {
    private final String fName = null;
    private final String lName = null;


    @Override
    public void doSomething() {
        System.out.println(fName + " " + lName);

    }
}
