package com.generics;

public class Main {
    public static void main(String[] args) {

        Integer[] arr = {2, 5, 6, 5, 7};
        Average<Integer> intArr = new Average<>(arr);
        System.out.println("Integer average = " + intArr.avr());

        Double[] arr2 = {2.3, 4.3, 1.5, 6.7, 4.8};
        Average<Double> doubleArr = new Average<>(arr2);
        System.out.println("Double average = " + doubleArr.avr());

    }
}
