package test.java.com.testing;
import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;


public class SortingClassTest {
    int[] nullArr = null;
    int[] emptyArr;
    int[] arr5 = new int[5];
    int[] expectedArr5 = new int[5];
    int[] sortedArr = {1, 2, 3, 4, 5};
    int[] sortedArr2 = {1, 2, 3, 4, 5};
    int[] longArr = {5, 3, 4, 2, 1, 6, 8, 7, 9, 10, 15, 12, 14, 13, 11};
    int[] sortedLongArr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};


    @Test(expected = NullPointerException.class)
    public void nullArraySortTest() {
        Arrays.sort(nullArr);
    }

    @Test(expected = NullPointerException.class)
    public void emptyArrSortTest() {

        Arrays.sort(emptyArr);
    }

    @Test
    public void arr5SortTest() {
        Arrays.sort(arr5);
        assertEquals(true, Arrays.equals(expectedArr5, arr5));
    }

    @Test
    public void sortedArrTest() {
        Arrays.sort(sortedArr);
        assertEquals(true, Arrays.equals(sortedArr2, sortedArr));
    }

    @Test(timeout = 10)
    public void sortedLongArrTest() {
        Arrays.sort(longArr);
        assertEquals(true, Arrays.equals(sortedLongArr, longArr));
    }
}