public class Main {
    public static void main(String[] args) {
        CakeFactory factory = new CakeFactory();

        Cake cherryCake = factory.getCake(CakeFactory.CakeTypes.CHERRY);
        Cake chocolateCake = factory.getCake(CakeFactory.CakeTypes.CHOCOLATE);


        cherryCake.Make();
        chocolateCake.Make();


    }
}
