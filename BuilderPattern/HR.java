public class HR {
    private PersonBuilder personBuilder;

    public void setPersonBuilder(PersonBuilder pb) {
        personBuilder = pb;
    }

    public Person getPerson() {
        return personBuilder.getPerson();
    }

    public void construcPerson() {

        personBuilder.createNewPerson();
        personBuilder.createFName();
        personBuilder.createLName();
        personBuilder.createSalary();
    }

}
