public class Person {
    private String fName;
    private String lName;
    private int salary;

    public void setFName(String fName) {
        this.fName = fName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getUserName() {
        String result = fName + " " + lName;
        return result;
    }

}