public class Lawer extends PersonBuilder {
    @Override
    public void createFName() {
        person.setFName("Ivan");
    }

    @Override
    public void createLName() {
        person.setlName("Ivanov");
    }

    @Override
    public void createSalary() {
        person.setSalary(20000);
    }
}
