public class Manager extends PersonBuilder {
    @Override
    public void createFName() {
        person.setFName("John");
    }

    @Override
    public void createLName() {
        person.setlName("Johnson");
    }

    @Override
    public void createSalary() {
        person.setSalary(35000);
    }
}
