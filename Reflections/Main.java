import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException, NoSuchFieldException {
        Class c = java.util.HashMap.class;

        HashMap<String, String> myHashMap = (HashMap<String, String>) c.getConstructor().newInstance();
        Class hashMapClass = myHashMap.getClass();

        Method method = hashMapClass.getMethod("put", Object.class, Object.class);
        method.invoke(myHashMap, "k1", "v1");
        method.invoke(myHashMap, "k2", "v2");
        method.invoke(myHashMap, "k3", "v3");

        System.out.println("k1 = " + hashMapClass.getMethod("get", Object.class).invoke(myHashMap, "key1"));
        System.out.println("k2 = " + hashMapClass.getMethod("get", Object.class).invoke(myHashMap, "key2"));
        System.out.println("k3 = " + hashMapClass.getMethod("get", Object.class).invoke(myHashMap, "key3"));

        if ("java.util.HashMap".equals(hashMapClass.getName())) {
            System.out.println("OK");
        } else {
            System.out.println("Inappropriate instance");
        }

        System.out.println("Object instance of class " + hashMapClass.getClass());
        System.out.println("Supper class " + hashMapClass.getSuperclass());

        Class[] interfaces = hashMapClass.getInterfaces();
        System.out.println("Implemented interfaces:");
        for (Class cInterface : interfaces) {
            System.out.println(cInterface.getName());
        }


        Class arrayListClass = ArrayList.class;
        Field[] pubFieldsArrayList = arrayListClass.getFields();
        System.out.println("Public fields of ArrayList:");
        for (Field field : pubFieldsArrayList) {
            System.out.println(field.getName());
        }

        System.out.println("");

        Method[] pubMethodArrayList = arrayListClass.getMethods();
        System.out.println("Public methods of ArrayList:");
        for (Method m : pubMethodArrayList) {
            System.out.println(m.getName());
        }

        System.out.println("");


        Field[] privFieldsArrayList = arrayListClass.getDeclaredFields();
        System.out.println("Private fields of ArrayList:");
        for (Field fieldPriv : privFieldsArrayList) {
            System.out.println(fieldPriv.getName());
        }

        System.out.println("");

        Method[] privMethodArrayList = arrayListClass.getDeclaredMethods();
        System.out.println("Private methods of ArrayList:");
        for (Method methodPriv : privMethodArrayList) {
            System.out.println(methodPriv.getName());
        }

        Class myClass = Class.forName("MyClass");
        MyClass m = new MyClass();
        System.out.println(myClass.getName());
        Field privField = myClass.getDeclaredField("privatValue");
        privField.setAccessible(true);
        privField.set(m, 100);
        System.out.println("New privatValue = " + (int) privField.get(m));

        String strErr = (String) privField.get(m);
        System.out.println(strErr);


    }
}
