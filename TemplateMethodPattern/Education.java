public abstract class Education {

    protected abstract void enter();

    protected abstract void study();

    protected abstract void passExam();

    protected abstract void getDocument();


    public final void learn() {
        enter();
        study();
        passExam();
        getDocument();
    }




}
