public class School extends Education {
    @Override
    protected void enter() {
        System.out.println("Pass entrance exams.");
    }

    @Override
    protected void study() {
        System.out.println("Do homework.");
    }

    @Override
    protected void passExam() {
        System.out.println("Pass exams.");
    }

    @Override
    protected void getDocument() {
        System.out.println("Get school certificate.");
    }
}
