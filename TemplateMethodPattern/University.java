public class University extends Education {
    @Override
    protected void enter() {
        System.out.println("Pass entrance exams and enter the University");
    }

    @Override
    protected void study() {
        System.out.println("Attend lectures.");
        System.out.println("Take an internship.");
    }

    @Override
    protected void passExam() {
        System.out.println("Pass exams and internship");
    }

    @Override
    protected void getDocument() {
        System.out.println("Get daploma.");

    }
}
