package com.generics;

import com.generics.Average;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AverageTest {
    private static Average nullObj = null;
    private static Average obj1;
    private static Average obj2;
    private static Average obj3;
    private static Average obj4;
    Number[] arr = {0, 0, 0, 0, 0, 0};
    Number[] arr2 = {5};
    Number[] arr3 = {3, 6, 8, 7, 3, 5, 9, 7, 8, 1};
    Number[] arr4 = new Number[5];


    @Before
    public void createObj() {
        obj1 = new Average(arr);
        obj2 = new Average(arr2);
        obj3 = new Average(arr3);
        obj4 = new Average(arr4);
    }


    @Test(expected = NullPointerException.class)
    public void nullObjAvr() {
        nullObj.avr();
    }

    @Test
    public void obj1Avr() {
        assertEquals(0.0, obj1.avr());
    }

    @Test
    public void obj2Avr() {
        assertEquals(5.0, obj2.avr());
    }

    @Test
    public void obj3Avr() {
        assertEquals(5.7, obj3.avr());
    }

    @Test (expected = NullPointerException.class)
    public void obj4Avr() {
       obj4.avr();
    }


}