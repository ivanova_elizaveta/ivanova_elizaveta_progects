package com.generics;

public class Average<T extends Number> {
    private T[] array;

    public Average(T[] array) {

        this.array = array;
    }

    public Number avr() {
        double sum = 0;
        Number res;
        for (T n : array) {
            sum += n.doubleValue();
        }
        res = sum / array.length;
        return res;
    }

}
