public class Main {
    public static void main(String[] args) {

        MyLinkedList<String> list = new MyLinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");

        System.out.println(list.size());
        System.out.println(list.isEmpty());
        System.out.println(list.contains("c"));

        MyLinkedList<String> list2 = new MyLinkedList<>();
        list2.add("e");
        list2.add("i");
        list2.add("f");

        System.out.println(list.addAll(list2));
        System.out.println(list.size());
    }
}
