
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


public class MyLinkedList<T> implements List<T> {
    private static class Node<T> {
        T info;
        Node<T> next;

        public Node(T info) {
            this.info = info;
        }
    }

    private Node<T> head;
    private int count = 0;


    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public boolean contains(Object o) {
        boolean res = false;
        Node<T> t = head;
        while (t != null) {
            if (t.info.equals(o)) {
                res = true;
                break;
            }
            t = t.next;
        }
        return res;
    }

    @Override
    public Iterator<T> iterator() {

        Iterator<T> iterator = new Iterator<T>() {
            int cursor;

            @Override
            public boolean hasNext() {
                return cursor < count;
            }

            @Override
            public T next() {
                if (cursor >= count) {
                    throw new IndexOutOfBoundsException("Index is bigger then list size.");
                }
                return get(cursor++);
            }
        };
        return iterator;

    }

    @Override
    public Object[] toArray() {
        Object[] arr = new Object[count];
        Node<T> currentNode = head;
        for (int i = 0; i < count; i++) {
            arr[i] = currentNode.info;
            currentNode = currentNode.next;
        }
        return arr;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        Object[] arr = toArray();
        System.arraycopy(a, 0, arr, count - 1, a.length);
        this.count += a.length;
        return a;
    }

    @Override
    public boolean add(T t) {
        final Node<T> newNode = new Node<>(t);
        if (head == null) {
            head = newNode;
        } else {
            Node<T> currentNode = head;
            for (int i = 0; i < count; i++) {
                if (currentNode.next == null) {
                    break;
                }
                currentNode = currentNode.next;
            }
            currentNode.next = newNode;
            count++;
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        boolean result = false;
        if (head != null) {
            Node<T> currentNode = head;
            Node<T> prevNode = currentNode;
            for (int i = 0; i < count; i++) {
                if (currentNode.next == null) {
                    break;
                } else {
                    if (currentNode.info.equals(o)) {
                        prevNode.next = currentNode.next;
                        result = true;
                        count--;
                        break;
                    }
                }
                prevNode = currentNode;
                currentNode = currentNode.next;
            }
        }
        return result;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean result = true;
        if (count == 0) {
            throw new IndexOutOfBoundsException("List is empty.");
        }
        for (Object o : c) {
            if (!contains(o)) {
                result = false;
                break;
            }
        }
        return result;

    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean result = true;
        for (T item : c) {
            result = add(item);
            if (!result) {
                break;
            }
        }
        return result;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        boolean result = true;
        for (T item : c) {
            add(index, item);
            index++;
        }
        return result;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean result = true;
        for (Object o : c) {
            if (o != null) {
                result = remove(o);
                if (!result) {
                    break;
                }
            } else {
                break;
            }
        }
        return result;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean result = true;
        Node<T> currentNode = head;
        for (int i = 0; i < count; i++) {
            if (!c.contains(currentNode.info) && currentNode.next != null) {
                result = remove(currentNode.info);
                if (!result) {
                    break;
                }
            }
        }
        return result;

    }

    @Override
    public void clear() {
        head = null;
        count = 0;
    }

    @Override
    public T get(int index) {
        T result = null;
        if (head != null) {
            Node<T> currentNode = head;
            for (int i = 0; i < count; i++) {
                if (currentNode.next == null || index == count - 1) {
                    break;
                }
                currentNode = currentNode.next;
            }
            result = currentNode.info;
        }
        return result;
    }

    @Override
    public T set(int index, T element) {
        Node<T> currentNode = head;
        for (int j = 0; j <= index; j++) {
            if (j == index) {
                Node<T> currentNodePrev = currentNode;
                currentNode.info = element;
                return currentNodePrev.info;
            }
            currentNode = currentNode.next;
        }
        return null;

    }

    @Override
    public void add(int index, T element) {
        if (index > count) {
            throw new IndexOutOfBoundsException("Index is bigger then list size.");
        }
        Node<T> currentNode = head;
        Node<T> newNode = new Node<>(element);
        if (index == 0) {
            head = newNode;
            newNode = currentNode.next;
        }
        for (int j = 0; j <= index; j++) {
            if (j == index - 1) {
                Node<T> prevNode = currentNode.next;
                currentNode.next = newNode;
                newNode.next = prevNode;
                count++;
            }
            if (currentNode.next == null) {
                break;
            }
            currentNode = currentNode.next;
        }
    }

    @Override
    public T remove(int index) {
        if (index > count) {
            throw new IndexOutOfBoundsException("Index is bigger then list size.");
        }
        Node<T> currentNode = head;
        Node<T> prevNode = head;
        for (int j = 0; j < this.count; j++) {
            if (currentNode.next == null) {
                break;
            } else if (index == j) {
                prevNode.next = currentNode.next;
                count--;
                break;
            }
            prevNode = currentNode;
            currentNode = currentNode.next;
        }
        return currentNode.info;
    }

    @Override
    public int indexOf(Object o) {
        int res = -1;
        Node<T> currentNode = head;
        for (int j = 0; j < count; j++) {
            if (currentNode.info != null && currentNode.info == o) {
                res = j;
                break;
            }
            currentNode = currentNode.next;
        }
        return res;
    }

    @Override
    public int lastIndexOf(Object o) {
        int res = -1;
        Node<T> currentNode = head;
        for (int j = 0; j < count; j++) {
            if (currentNode.info != null && currentNode.info == o) {
                res = j;
            }
            currentNode = currentNode.next;
        }
        return res;
    }

    @Override
    public ListIterator<T> listIterator() {
        ListIterator<T> listIterator = new ListIterator<T>() {
            int cursor = 0;

            @Override
            public boolean hasNext() {
                return cursor < count;
            }

            @Override
            public T next() {
                if (cursor >= count) {
                    throw new IndexOutOfBoundsException("Index is bigger then list size.");
                }
                return get(cursor++);
            }

            @Override
            public boolean hasPrevious() {
                return cursor != 0;
            }

            @Override
            public T previous() {
                if (cursor >= count || count == 0 || cursor == 0) {
                    throw new IndexOutOfBoundsException("Index is bigger then list size.");
                }
                return get(cursor--);
            }

            @Override
            public int nextIndex() {
                return cursor + 1;
            }

            @Override
            public int previousIndex() {
                return cursor - 1;
            }

            @Override
            public void remove() {
                MyLinkedList.this.remove(cursor);
            }

            @Override
            public void set(T t) {
                MyLinkedList.this.set(cursor, t);
            }

            @Override
            public void add(T t) {
                MyLinkedList.this.add(t);
            }
        };
        return listIterator;

    }

    @Override
    public ListIterator<T> listIterator(int index) {
        ListIterator<T> listIterator = new ListIterator<T>() {
            int cursor = index;

            @Override
            public boolean hasNext() {
                return cursor < count;
            }

            @Override
            public T next() {
                if (cursor >= count) {
                    throw new IndexOutOfBoundsException("Index is bigger then list size.");
                }
                return get(cursor++);
            }

            @Override
            public boolean hasPrevious() {
                return cursor != 0;
            }

            @Override
            public T previous() {
                if (cursor >= count || count == 0) {
                    throw new IndexOutOfBoundsException("Index is bigger then list size.");
                }
                return get(cursor--);
            }

            @Override
            public int nextIndex() {
                return cursor + 1;
            }

            @Override
            public int previousIndex() {
                return cursor - 1;
            }

            @Override
            public void remove() {
                MyLinkedList.this.remove(cursor);
            }

            @Override
            public void set(T t) {
                MyLinkedList.this.set(cursor, t);
            }

            @Override
            public void add(T t) {
                MyLinkedList.this.add(t);
            }
        };
        return listIterator;

    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (fromIndex > count || toIndex > count || count == 0) {
            throw new IndexOutOfBoundsException("Empty list or size list < index");
        }
        Node<T> currentNode = this.head;
        MyLinkedList<T> result = new MyLinkedList<>();
        for (int j = 0; j < this.count; j++) {
            if (fromIndex == j) {
                result.head = new Node<>(currentNode.info);
            } else if (fromIndex < j && j <= toIndex) {
                result.add((T) new Node<Object>(currentNode.info));
                if (currentNode.next == null) {
                    break;
                }
                if (j == toIndex) {
                    Object ob = new Object();
                    result.add((T) ob);
                    result.remove(result.count);
                    break;
                }
            }
            currentNode = currentNode.next;
        }
        return result;
    }
}
