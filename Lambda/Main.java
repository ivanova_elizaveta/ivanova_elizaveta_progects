import java.util.HashMap;
import java.util.Map;
import java.text.*;
import java.util.concurrent.atomic.DoubleAccumulator;

public class Main {
    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("#.##");
        Calculate<Double> myCalc = (a, b) -> a - b;
        System.out.println("res= " + df.format(myCalc.exec(34.8, 17.59)));
        System.out.println();

        System.out.println("mul= " + df.format(myCalc.mul(30d, 55d)));
        System.out.println("div= " + df.format(myCalc.div(30d, 55d)));
        System.out.println();


        Map<String, Calculate<Double>> calcMap = new HashMap<>();
        calcMap.put("+", (a, b) -> a + b);
        calcMap.put("-", (a, b) -> a - b);
        calcMap.put("*", (a, b) -> a * b);
        calcMap.put("/", (a, b) -> a / b);


        String operation = "+";
        System.out.println("sum = " + df.format(calcMap.get(operation).exec(34.8, 17.59)));
        operation = "-";
        System.out.println("minus = " + df.format(calcMap.get(operation).exec(34.8, 17.59)));
        operation = "*";
        System.out.println("mul = " + df.format(calcMap.get(operation).exec(34.8, 17.59)));
        operation = "/";
        System.out.println("div = " + df.format(calcMap.get(operation).exec(34.8, 17.59)));

    }
}

