@FunctionalInterface
interface Calculate<T extends Number> {

    public T exec(T a, T b);

    default Double sum(T a, T b) {
        return a.doubleValue() + b.doubleValue();
    }

    default Double minus(T a, T b) {
        return a.doubleValue() - b.doubleValue();
    }

    default Double mul(T a, T b) {
        return a.doubleValue() * b.doubleValue();
    }

    default Double div(T a, T b) {
        return a.doubleValue() / b.doubleValue();
    }


}
