import java.util.*;

public class Main {
    public static void main(String[] args) {
        User u1 = new User("User1", 22);
        User u2 = new User("User2", 45);
        User u3 = new User("User3", 15);

        List<User> users = new ArrayList<>();
        try {
            users.add(u1);
            users.add(u2);
            users.add(u3);
        } catch (ClassCastException e) {
            System.out.println("ClassCastException");
        }

        for (User u : users) {
            System.out.println(u.getName() + " " + u.getAge());
        }

        Collection<User> checkedlist = Collections.checkedList(users, User.class);
        Collection<User> synchList = Collections.synchronizedList(users);

        Map<Integer, User> usersMap = new HashMap<>();

        usersMap.put(1, u1);
        usersMap.put(2, u2);
        usersMap.put(3, u3);

        Map<Integer, User> checkedMap = Collections.checkedMap(usersMap, Integer.class, User.class);


    }
}
