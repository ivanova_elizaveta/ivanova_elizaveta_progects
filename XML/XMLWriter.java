import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class XMLWriter {
    public static void main(String[] args) {
        String homeDir = System.getProperty("user.home");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;

        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();
            Element rootElement =
                    doc.createElementNS("http://google.com", "User");
            doc.appendChild(rootElement);
            rootElement.appendChild(getUser(doc, "1", "Ivan", "21"));
            rootElement.appendChild(getUser(doc, "2", "Petro", "32"));
            rootElement.appendChild(getUser(doc, "3", "Maria", "45"));

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);

            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File(homeDir + File.separator + "users.xml"));

            transformer.transform(source, console);
            transformer.transform(source, file);
            System.out.println("XML file is created");

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private static Node getUser(Document doc, String id, String name, String age) {
        Element user = doc.createElement("User");

        user.setAttribute("id", id);

        user.appendChild(getUserElements(doc, user, "name", name));

        user.appendChild(getUserElements(doc, user, "age", age));
        return user;
    }

    private static Node getUserElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }
}
