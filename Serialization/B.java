import java.io.Serializable;

public class B extends A implements Serializable {
    private static final long serialVersionUID = -6862926644813433707L;

    public int b1 = 0;
    public transient int b2 = 0;
    C c;


    public B(int a1, int a2, char c, int b1, int b2, C c1) {
        super(a1, a2, c);
        this.b1 = b1;
        this.b2 = b2;
        this.c = c1;
    }


}
