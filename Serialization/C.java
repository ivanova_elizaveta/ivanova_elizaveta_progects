import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class C implements Serializable {
    private static final long serialVersionUID = -6862926644813433707L;

    public int c1 = 0;
    public transient byte c2 = 0;



    private static final String DATA_PATH = System.getProperty("user.home");

    public static void save(List<Serializable> serializableList) throws IOException {
        for (Serializable serializable : serializableList) {
            final Path path = Paths.get(DATA_PATH, serializable.getClass().getName(), UUID.randomUUID().toString());
            boolean mkdirs = path.toFile().createNewFile();
            try (ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream(path.toString()))) {
                oos.writeObject(serializable);
                oos.flush();
            } catch (IOException ex) {
                Files.delete(path);
                System.out.println(ex);
            }
        }
    }

    public static List<Serializable> load(Class<? extends Serializable> clazz) {
        final List<Serializable> result = new ArrayList<>();
        final Path dirPath = Paths.get(DATA_PATH, clazz.getName());
        for (File file : dirPath.toFile().listFiles()) {
            if (file.isFile()) {
                try (ObjectInputStream oin = new ObjectInputStream(
                        new FileInputStream(file))) {
                    result.add((Serializable) oin.readObject());
                } catch (IOException | ClassNotFoundException ex) {
                    System.out.println(ex);
                }
            }
        }
        return result;
    }

}



