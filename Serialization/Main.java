import java.io.IOException;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws IOException {
        A a1 = new A(2, 456, 'a');
        A a2 = new A(55, 876, 'd');
        C c = new C();


        B b1 = new B(3, 4, 's', 43, 15, c);
        B b2 = new B(4, 5, 't', 58, 21, c);


        c.save(Arrays.asList(a1, a2));
        c.load(c.getClass());


    }
}
