import java.io.Serializable;

public class A implements Serializable {
    private static final long serialVersionUID = -6862926644813433707L;

    public int a1 = 0;
    public int a2 = 0;
    public transient char c = ' ';

    public A(int a1, int a2, char c) {
        this.a1 = a1;
        this.a2 = a2;
        this.c = c;
    }
}
